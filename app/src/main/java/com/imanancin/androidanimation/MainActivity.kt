package com.imanancin.androidanimation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Animation Utils

        val image = findViewById<ImageView>(R.id.imageView)

        val fadein = findViewById<Button>(R.id.fadein)
        val fadeout = findViewById<Button>(R.id.fadeout)
        val zoomin = findViewById<Button>(R.id.zoomin)
        val zoomout = findViewById<Button>(R.id.zoomout)
        val moveright = findViewById<Button>(R.id.moveright)
        val moveleft = findViewById<Button>(R.id.moveleft)
        val cw = findViewById<Button>(R.id.clockwisee)
        val cc = findViewById<Button>(R.id.cc)

        fadein.setOnClickListener {
            val anim = AnimationUtils.loadAnimation(this, R.anim.fade_in)
            image.startAnimation(anim)
        }

        fadeout.setOnClickListener {
            val anim = AnimationUtils.loadAnimation(this, R.anim.fade_out)
            image.startAnimation(anim)
        }

        zoomin.setOnClickListener {
            val anim = AnimationUtils.loadAnimation(this, R.anim.zoom_in)
            image.startAnimation(anim)
        }

        zoomout.setOnClickListener {
            val anim = AnimationUtils.loadAnimation(this, R.anim.zoom_out)
            image.startAnimation(anim)
        }

        moveright.setOnClickListener {
            val anim = AnimationUtils.loadAnimation(this, R.anim.move_right)
            image.startAnimation(anim)
        }

        moveleft.setOnClickListener {
            val anim = AnimationUtils.loadAnimation(this, R.anim.move_left)
            image.startAnimation(anim)
        }
        cw.setOnClickListener {
                    val anim = AnimationUtils.loadAnimation(this, R.anim.clockwise)
                    image.startAnimation(anim)
        }
        cc.setOnClickListener {
                    val anim = AnimationUtils.loadAnimation(this, R.anim.counter_clockwise)
                    image.startAnimation(anim)
        }

    }
}